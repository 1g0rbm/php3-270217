<?php

namespace core;

class Config extends Container
{
    private $settings;

    public function init(array $settings)
    {
        $this->settings = $settings;
    }

    public function getSetting($path)
    {
        $path = explode('.', $path);
        $setting = $this->settings;

        foreach ($path as $key) {
            if (isset($setting[$key])) {
                $setting = $setting[$key];
            } else {
                $setting = false;
                break;
            }
        }

        return $setting;
    }
}