<?php

namespace core;

use \core\exceptions\PageNotFoundException;

class App extends Container
{
    public function __construct(array $array)
    {
        parent::__construct($array);
        $this['service.provider'] = new ServiceProvider($this);
    }

    public function get($uri, $callback)
    {
        $this['service.provider']->getService('Router')->setRout($uri, $callback, Request::METHOD_GET);
    }

    public function post($uri, $callback)
    {
        $this['service.provider']->getService('Router')->setRout($uri, $callback, Request::METHOD_POST);
    }

    public function run()
    {
        try {
            $route = $this['service.provider']->getService('Router')->getRoute();
        } catch (PageNotFoundException $e) {
            echo $e->getMessage();
            die;
        }

        call_user_func_array($route['callback'], $route['params']);
    }
}