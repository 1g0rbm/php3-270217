<?php

namespace core;

use \core\exceptions\PageNotFoundException;

class Router extends Container
{
    private $storage = [];

    public function setRout($rout, $callback, $method)
    {
        $rout = $this->routHandler($rout);

        $this->storage[$method][$rout['mask']]['callback'] = $callback;
        $this->storage[$method][$rout['mask']]['params'] = $rout['params'];
    }

    public function getRoute()
    {
        return $this->rebuildRoute($this['service.provider']->getService('Request')->getUri());
    }

    private function rebuildRoute($rout)
    {
        $rout = $this->splitQuery($rout);

        $routPartial = explode('/', $this->sliceLastDelimetr($rout['rout']));

        $wantedRout = false;
        $buffer = [];

        do {
            $a = array_shift($routPartial);

            $buffer[] = $a;
            $mask = implode('/', $buffer) . count($routPartial);
            $method = $this['service.provider']->getService('Request')->methodIs();

            if (isset($this->storage[$method][$mask])) {
                $wantedRout = $this->storage[$method][$mask];
                $wantedRout['params'] = $routPartial;
            }

        } while ($a !== null);

        if (!$wantedRout) {
            throw new PageNotFoundException();
        }

        return $wantedRout;
    }

    private function routHandler($route)
    {
        $routePartial = explode('/', $route);
        $withoutParams = [];
        $params = [];

        foreach ($routePartial as $key => $value) {
            if (strpos($value, '}')) {
                $params[] = substr($value, 1, -1);
            } else {
                $routePartial[$key] = $value;
                $withoutParams[$key] = $value;
            }
        }

        return [
            'params' => $params,
            'mask' => implode('/', $withoutParams) . count($params)
        ];
    }

    private function splitQuery($rout)
    {
        $buffer = explode('?', $rout);

        return [
            'rout' => $buffer[0],
            'parameters' => $buffer[1] ?? null
        ];
    }

    private function sliceLastDelimetr($route)
    {
        if ($route[strlen($route) - 1] === '/') {
            return substr($route, 0, -1);
        }

        return $route;
    }
}