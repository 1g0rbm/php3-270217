<?php

namespace core\exceptions;

class PageNotFoundException extends \Exception
{
	public function __construct($message = 'Page Not Found', $code = 404)
	{
		header("HTTP/1.1 404 Not Found");
		parent::__construct($message, $code);
	}
}