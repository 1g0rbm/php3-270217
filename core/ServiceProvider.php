<?php

namespace core;

class ServiceProvider
{
    const SERVICE_NAMESPACE = 'core\\';

    private $instanceStorage = [];
    private $app;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->app['Config'] = $this->getService('Config', [$app['settings']]);
    }

    public function getService($name, array $params = [])
    {
        $name = $this->getFullyClassName($name);

        if (!isset($this->instanceStorage[$name])) {
            $this->instanceStorage[$name] = new $name;
            $this->instanceStorage[$name]['service.provider'] = $this;

            if (method_exists($this->instanceStorage[$name], 'init')) {
                call_user_func_array([$this->instanceStorage[$name], 'init'], $params);
            }
        }

        return $this->instanceStorage[$name];
    }

    public function getCountInitServices(){
        return count($this->instanceStorage);
    }

    private function getFullyClassName($name)
    {
        return self::SERVICE_NAMESPACE . $name;
    }

    private function injectDependency($serivceName)
    {
//        $serivces = services.php;
//        isset($serivceName);
//        $serviceSettings;

//            $res = []
//        foreach ($serviceSettings['dependencys'] as $key => value) {
//                explode(':' $value)

//                switch(){
//                    case: model,
//                    case: service $this->getService();
//                }

//             $res[$key] =
//        }
    }
}