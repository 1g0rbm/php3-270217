<?php

namespace models;

use core\SQL;

abstract class Base
{
    protected $db;
    protected $table;
    protected $prim_key;

    public function __construct(SQL $db)
    {
        $this->db = $db;
    }

    public function all()
    {
        return $this->db->select("SELECT * FROM {$this->table}");
    }

    public function one($id)
    {
        $res = $this->db->select("SELECT * FROM {$this->table} WHERE {$this->prim_key}='$id'");

        if (empty($res)) {
            return $res;
        }

        return $res[0];
    }

    public function delete($id)
    {
        $id = (int)$id;
        $where = "{$this->prim_key}='$id'";
        return $this->db->delete($this->table, $where);
    }

    public function add($params)
    {
        $id = $this->db->insert($this->table, $params);

        return $id;
    }

    public function edit($id, $params)
    {
        $where = "{$this->prim_key}='$id'";
        return $this->db->update($this->table, $params, $where);
    }
}