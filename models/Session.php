<?php

namespace models;

use core\SQL;

class Session extends Base
{
    public function __construct(SQL $db)
    {
        parent::__construct($db);
        $this->table = 'user_session';
        $this->prim_key = 'sid';
    }

    public function clearOld()
    {
        $min = date('Y-m-d H:i:s', time() - 60 * 20);
        $where = "last_modified < '$min'";
        $this->db->delete($this->table, $where);
    }
}