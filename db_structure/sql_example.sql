SELECT
  name,
  (SELECT count(privilege.id_priv)
   FROM privilege
     JOIN priv2role USING(id_priv)
   WHERE role.id_role = priv2role.id_role) AS cnt_priv
FROM role;

/***** *****/

SELECT
  name,
  (SELECT count(privilege.id_priv)
   FROM privilege
     JOIN priv2role USING(id_priv)
   WHERE role.id_role = priv2role.id_role) AS cnt_priv,
  IF((SELECT count(privilege.id_priv)
      FROM privilege
        JOIN priv2role USING(id_priv)
      WHERE role.id_role = priv2role.id_role) >= 3, (SELECT count(privilege.id_priv)
                                                     FROM privilege
                                                       JOIN priv2role USING(id_priv)
                                                     WHERE role.id_role = priv2role.id_role), 'none')
FROM role;

/***** *****/

SET @cnt_privs = NULL;

SELECT
  name,
  @cnt_privs := (SELECT count(privilege.id_priv)
                 FROM privilege
                   JOIN priv2role USING (id_priv)
                 WHERE role.id_role = priv2role.id_role) AS cnt_priv,
  IF(@cnt_privs >= 3, @cnt_privs, 'none')                AS if_example
FROM role;