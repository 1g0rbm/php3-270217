create table privilege
(
	id_priv int not null auto_increment
		primary key,
	name varchar(55) null,
	description varchar(255) null,
	constraint privilege_name_uindex
		unique (name)
)
;