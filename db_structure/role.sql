create table role
(
	id_role int not null auto_increment
		primary key,
	name varchar(50) null,
	description varchar(255) null,
	constraint roles_name_uindex
		unique (name)
)
;

