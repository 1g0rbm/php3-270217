<?php
return [
    'User' => [
        'dependency' => [
            'db' => 'service:SQL',
            'mUser' => 'model:User',
            'mSession' => 'model:Session',
        ]
    ]
];